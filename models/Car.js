const mongoose = require('mongoose');

const schema = new mongoose.Schema({
  name: {
    type: String,
    required: true
  },
  power: Number,
  color: String
});

module.exports = mongoose.model('Car', schema);
