const checkCar = (name, power, color) => {
  const error = {};
  if (name === '' || name === undefined || name === null) {
    error.name = "The name can't be blank";
  } else {
    if (String(name).length < 5) {
      error.name = 'The name should not be shorter than 5 characters';
    }
  }

  if (power === '' || name === undefined || name === null || power === 0) {
    error.power = 'Power should be specified';
  } else {
    if (Number(power) < 0) {
      error.power = 'Power cannot be negative';
    } else if (Number(power) > 8000) {
      error.power = 'Power can not be more than 8000';
    }
  }

  if (color === '' || color === undefined || color === null || color === 0) {
    error.color = 'Color should be specified';
  } else {
    if (color !== 'blue' && color !== 'red') {
      error.color = 'Color can only be red or blue';
    }
  }
  return error;
};

module.exports = checkCar;
