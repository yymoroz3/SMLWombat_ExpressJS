const rolePermission = role => (req, res, next) => {
  if (req.session.user.role !== role) {
    return res.render('400');
  }
  next();
};

module.exports = rolePermission;
