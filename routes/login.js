const express = require('express');
const User = require('../models/User');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('login', { error: null });
});

router.post('/', function(req, res, next) {
  const { login, password } = req.body;
  User.findOne({ login, password })
    .then(user => {
      if (user) {
        req.session.user = user;
        res.redirect('/');
      } else {
        res.render('login', { error: 'Incorrect login / password' });
      }
    })
    .catch(next);
});

module.exports = router;
