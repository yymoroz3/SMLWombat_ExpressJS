const express = require('express');
const User = require('../models/User');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  req.session.user = void 0;
  res.redirect('/login');
});

module.exports = router;
