const express = require('express');
const Car = require('../models/Car');
const carValidation = require('../validations/carValidation');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('add', { error: {}, car: {}, viewError: false });
});

router.post('/', function(req, res, next) {
  const { name, power, color } = req.body;
  const error = carValidation(name, power, color);
  if (Object.keys(error).length > 0) {
    return res.render('add', { error, car: req.body, viewError: true });
  }
  const newCar = new Car({
    name,
    power,
    color,
  });
  newCar
    .save()
    .then(_ => {
      res.redirect('/');
    })
    .catch(err => {
      console.error(err);
      next(err);
    });
});

module.exports = router;
