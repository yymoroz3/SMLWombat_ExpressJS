const express = require('express');
const Car = require('../models/Car');
const carValidation = require('../validations/carValidation');
const router = express.Router();

/* GET cars. */
router.get('/cars', function(req, res, next) {
    Car.find({}).then(carList => {
        res.json(carList);
    });
});

// add new cars
router.post('/cars', function(req, res, next) {
    const { name, power, color } = req.body;
    const error = carValidation(name, power, color);
    if (Object.keys(error).length > 0) {
        return res.json({ error, car: req.body });
    }
    const newCar = new Car({
        name,
        power,
        color,
    });
    newCar
        .save()
        .then(_ => {
            res.json({ result: 'success' });
        })
        .catch(err => {
            res.json({ result: 'error', error: err });
        });
});

module.exports = router;
