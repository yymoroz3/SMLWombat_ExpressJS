const express = require('express');
const User = require('../models/User');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('signup', { error: null });
});

router.post('/', function(req, res, next) {
  const { login, password, role } = req.body;
  User.findOne({ login, password, role })
    .then(user => {
      if (user) {
        res.render('signup', { error: 'User exist' });
      } else {
        const newUser = new User({ login, password, role });
        return newUser.save();
      }
    })
    .then(newUser => {
      req.session.user = newUser;
      res.redirect('/');
    })
    .catch(next);
});

module.exports = router;
