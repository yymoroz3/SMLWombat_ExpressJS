const express = require('express');
const Car = require('../models/Car');
const router = express.Router();

/* GET home page. */
router.get('/:id', function(req, res, next) {
  Car.findById(req.params.id).then(car => {
    if (car) {
      return res.render('delete', { name: car.name, id: req.params.id });
    }
    return res.render('404');
  });
});

router.post('/:id', function(req, res, next) {
  const { id } = req.params;
  Car.findById(id)
    .then(car => {
      return car.remove();
    })
    .then(() => res.redirect('/'))
    .catch(next);
});

module.exports = router;
