const express = require('express');
const Car = require('../models/Car');
const router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  Car.find({}).then(carList => {
    res.render('index', { Cars: carList });
  });
});

module.exports = router;
