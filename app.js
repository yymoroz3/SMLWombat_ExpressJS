const express = require('express');
const path = require('path');
const favicon = require('serve-favicon');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const morgan = require('morgan');
const fs = require('fs');
const session = require('express-session');

const logger = require('./utils/logger');
const auth = require('./middleware/auth');
const roleAccess = require('./middleware/400');
const error404 = require('./routes/404');

const app = express();

if (!fs.existsSync('./logs')) {
    fs.mkdirSync('./logs');
}
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.use(
    session({
        secret: 'hello my friend',
        resave: true,
    }),
);

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(morgan('dev', { stream: logger.stream }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

const index = require('./routes/index');
const add = require('./routes/add');
const deleteRouter = require('./routes/delete');
const change = require('./routes/change');
const login = require('./routes/login');
const signup = require('./routes/signup');
const logout = require('./routes/logout');
const api = require('./routes/api');

app.use('/login', login);
app.use('/logout', logout);
app.use('/signup', signup);
app.use('/add', auth, roleAccess('admin'), add);
app.use('/delete', auth, roleAccess('admin'), deleteRouter);
app.use('/change', auth, change);
app.use('/api', api);
app.use('/', auth, index);

// 404 error
app.use(error404);

// error handler
app.use(function(err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
